/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_41;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
  public static void main(String[] args) {
    String dirName = System.getProperty("user.home")+"/UTP6dir";
    System.out.println(dirName);
    String resultFileName = "UTP6res.txt";
    Futil.processDir(dirName, resultFileName);


  }
}

package utp_zaoczne_41;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;

public class Futil implements FileVisitor<Path> {
    public static List<String> lines;

    public static File processDir(String dirname, String resultFileName)  {
        Futil visitor = new Futil();
        try {
            Files.walkFileTree(Paths.get(dirname), visitor);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String outputDirectoryPath = dirname + "UTP6res.txt";
        File output = new File(resultFileName);
        Path file = Paths.get(outputDirectoryPath);
//        try {
//            Files.write(file, lines, Charset.forName("UTF-8"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        return null;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }


    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        String fileName = file.getFileName().toString();
        //String separator =
        String path = file.getParent() + "\\" + fileName;
        String coding = String.valueOf(file.getFileSystem());

        System.out.println(path);
        System.out.println(fileName);


        try {
            List<String> allLines = Files.readAllLines(Paths.get(path));
            for(String line : allLines) {
                System.out.println(line);
//                lines.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return FileVisitResult.CONTINUE;
    }


    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        System.out.println("Failed to access file: " + file.toString());
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }




//    try {
//        Files.find(Paths.get(dirName),
//                Integer.MAX_VALUE,
//                (filePath, fileAttr) -> fileAttr.isRegularFile())
//                .forEach(System.out::println);
//    } catch (
//    IOException e) {
//        e.printStackTrace();
//    }
}
